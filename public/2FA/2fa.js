var c = base64DecToArr('dG80aG9oNlF1ZWV2MEFoeTFvaHphdTl1aXBhaGRlZTMK');

navigator.credentials.create({
    publicKey: {
        challenge: c,
        rp: {
            name: 'test',
        },
        user: {
            id: c,
            name: 'test',
            displayName: 'Test User',
        },
        authenticatorSelection: {
            userVerification: 'preferred',
        },
        attestation: 'direct',
        pubKeyCredParams: [{
            type: 'public-key',
            alg: -7,
        }, {
            type: 'public-key',
            alg: -257,
        }],
    }
}).then(function(res) {
    console.log(res);
}).catch(function(err) {
    console.log(err);
});

document.addEventListener('submit', function(event) {
    var form = event.target;
    event.preventDefault();
    navigator.credentials.get({
        publicKey: {
            challenge: c,
            userVerification: 'preferred',
        }
    }).then(function(res) {
        console.log(res);
    });
});
